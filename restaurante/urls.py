from django.contrib import admin
from django.urls import path
from .views import RestauranteView

urlpatterns = [
    path('', RestauranteView.as_view(), name="restaurante"),
]
