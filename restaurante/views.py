from django.shortcuts import render
from django.views import View

# Create your views here.
from django.views.generic import TemplateView

from restaurante.models import Platillo

class RestauranteView(View):
    template_name = "restaurante/restaurante.html"
    def get(self, request):
        Eventos = Platillo.objects.all()
        return render(request, self.template_name, {"productos": Eventos})