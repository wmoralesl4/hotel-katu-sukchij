from django.contrib import admin
from . models import *
# Register your models here.


class PlatilloAdmin(admin.ModelAdmin):

    class Meta:
        model = Platillo
        list_filter = ['platillos']


class MesaAdmin(admin.ModelAdmin):

    class Meta:
        model = Mesa
        list_filter = ['mesa']


class PedidoAdmin(admin.ModelAdmin):
    list_display = [
        'numero', 'mesa', 'tipo_plato'
    ]

    list_filter=[
        'numero', 'mesa'
    ]
    search_fields = ['numero']


admin.site.register(Platillo, PlatilloAdmin)
admin.site.register(Pedido, PedidoAdmin)
admin.site.register(Mesa,MesaAdmin)