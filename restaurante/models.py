from django.db import models

# Create your models here.

class Mesa(models.Model):
    mesa = models.CharField("Número de mesa", max_length=100, unique=True)

    def __str__(self):
        return "%s" % (self.mesa)
    
    class Meta:
        db_table = "Mesa"
        verbose_name= "Número de mesa"
        verbose_name_plural = "Números de mesas"


class Platillo(models.Model):
    platillos = models.CharField("Platillos ", max_length=100, unique=True)
    imagen=models.ImageField(upload_to="platillos", null=True, blank=True)
    precio = models.PositiveBigIntegerField()
    





    def __str__(self):
        return "%s" % (self.platillos)
    
    class Meta:
        db_table = "Platillos"
        verbose_name= "Platillo"
        verbose_name_plural = "Platillos"



class Pedido(models.Model):
    numero = models.PositiveIntegerField("Número de pedido", unique=True)
    costo = models.PositiveBigIntegerField("Costo", default=100)
    descripcion = models.TextField("Descripción", max_length=300)
    mesa = models.ForeignKey(
        Mesa, on_delete=models.CASCADE, blank=False, null=True
    )
    tipo_plato = models.ForeignKey(
        Platillo, on_delete=models.CASCADE, blank=False, null=True
    )

    def __str__(self):
        return "%s" % (self.numero)
    
    class Meta:
        db_table = "Pedido"
        verbose_name= "Pedido"
        verbose_name_plural = "Pedidos"
