from django.shortcuts import render
from django.views import View

# Create your views here.
from django.views.generic import TemplateView

from eventos.models import Evento


class EventosView(View):
    template_name = "eventos/eventos.html"
    def get(self, request):
        Eventos = Evento.objects.all()
        return render(request, self.template_name, {"productos": Eventos})