from django.contrib import admin
from eventos.models import Evento, tipoevento

# Register your models here.

class TipoAdmin(admin.ModelAdmin):

    class Meta:
        model = tipoevento
        list_filter = ['tipo']

class EventoAdmin(admin.ModelAdmin):
    list_filter = ['nombre']
    list_display = ['nombre', 'horaevento', 'descripcion']
    search_fields = ['nombre']
    list_per_page = 15
    ordering = ['horaevento']


admin.site.register(Evento, EventoAdmin)
admin.site.register(tipoevento, TipoAdmin)