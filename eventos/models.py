from django.db import models

# Create your models here.

class tipoevento(models.Model):
    tipo = models.CharField("Tipo de evento", max_length=80, unique=True)


    def __str__(self):
        return "%s" % (self.tipo)
    
    class Meta:
        db_table = "tipoevento"
        verbose_name= "Tipo de evento"
        verbose_name_plural = "Tipos de eventos"


class Evento(models.Model):
    nombre = models.CharField("Nombre", max_length=100, unique=True)
    horaevento = models.DateTimeField('Ingrese la fecha y hora del evento', null=False, blank=False)
    costo = models.PositiveBigIntegerField("Costo", default=100)
    descripcion = models.TextField("Descripción", max_length=300)
    tipo_evento = models.ForeignKey(
        tipoevento, on_delete=models.CASCADE, blank=False, null=True
    )
    imagen=models.ImageField(upload_to="platillos", null=True, blank=True)
    def __str__(self):
        return "%s" % (self.nombre)
    
    class Meta:
        db_table = 'Evento'
        verbose_name = 'Evento'
        verbose_name_plural = 'Eventos'