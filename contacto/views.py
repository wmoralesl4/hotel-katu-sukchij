from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView


class ContactoView(TemplateView):
    template_name = "contacto/contacto.html"
