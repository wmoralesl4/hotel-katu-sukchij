from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy
from autenticacion.models import Perfil
from .forms import RegistroForm, PerfilForm
from crispy_forms.templatetags import crispy_forms_tags as crispy

class RegistroView(CreateView):
    template_name = 'autenticacion/login.html'
    form_class = RegistroForm
    success_url = reverse_lazy('inicio')  # Ajusta la URL a la que redirigir después del registro

    def form_valid(self, form):
        response = super().form_valid(form)
        login(self.request, self.object)
        return response

def perfil(request):
    # Asegúrate de que el usuario esté autenticado
    if not request.user.is_authenticated:
        return redirect('login')

    perfil, created = Perfil.objects.get_or_create(user=request.user)

    if request.method == 'POST':
        perfil_form = PerfilForm(request.POST, instance=perfil)
        if perfil_form.is_valid():
            perfil_form.save()
    else:
        perfil_form = PerfilForm(instance=perfil)

    return render(request, 'perfil.html', {'perfil_form': perfil_form})