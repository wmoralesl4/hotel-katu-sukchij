from django.urls import path
from django.contrib import admin
from .views import RegistroView, perfil
from . import views


app_name = 'autenticacion'  # Agrega el namespace

urlpatterns = [
    path('registro/', RegistroView.as_view(), name='registro'),  # Este es un ejemplo, ajusta según tus URLs
    # Otras URLs...
]