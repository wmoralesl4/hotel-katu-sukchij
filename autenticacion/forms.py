from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import Perfil
from django.contrib.auth.models import User
from crispy_forms.templatetags import crispy_forms_tags as crispy

class RegistroForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

class PerfilForm(forms.ModelForm):
    class Meta:
        model = Perfil
        fields = [] 