from django.db import models
from django.contrib.auth.models import User

class Perfil(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # Agrega otros campos de perfil si es necesario

    def __str__(self):
        return self.user.username