from django.contrib import admin
from django.views import View
from .models import tipohabitacion, Habitacion
# Register your models here.


class tipo_habi(admin.ModelAdmin):

    class Meta:
        model = tipohabitacion
        list_filter = ['tipo']


class habitacion_Admin(admin.ModelAdmin):
    list_display = [
        'image_img','nombre', 'tipo_habitacion', 'costo'
    ]

    list_filter=[
        'nombre', 'tipo_habitacion'
    ]
    search_fields = ['nombre']


admin.site.register(Habitacion, habitacion_Admin)
admin.site.register(tipohabitacion, tipo_habi)