from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.views import View
from .models import tipohabitacion, Habitacion
from django.http import HttpResponseRedirect, HttpResponse

    
class HabitacionView(View):
    template_name = "habitacion/habitacion.html"
    def get(self, request):
        Eventos = Habitacion.objects.all()
        return render(request, self.template_name, {"habitaciones": Eventos})