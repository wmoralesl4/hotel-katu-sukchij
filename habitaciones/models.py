from django.db import models
from django.utils.safestring import mark_safe
# Create your models here.


class tipohabitacion(models.Model):
    tipo = models.CharField("Tipo de habitacion", max_length=80, unique=True)


    def __str__(self):
        return "%s" % (self.tipo)
    
    class Meta:
        db_table = "tipohabitacion"
        verbose_name= "Tipo de habitación"
        verbose_name_plural = "Tipos de habitaciones"


class Habitacion(models.Model):
    image = models.ImageField('Ingrese imagen', upload_to='fotos/habitaciones/',
        null=True, blank=True)
    nombre = models.CharField("Nombre", max_length=100, unique=True)
    costo = models.PositiveBigIntegerField("Costo", default=100)
    descripcion = models.TextField("Descripción", max_length=300)

    tipo_habitacion = models.ForeignKey(
        tipohabitacion, on_delete=models.CASCADE, blank=False, null=True
    )


    def image_img(self):
        if self.image:
            return mark_safe(
                '<img src="{url}" width="{width}" height={height} />'.format(
                    url=self.image.url, width=50, height=50))
        else:
            return 'No existe imagen'
    image_img.short_descripcion = "Imagen"
    image_img.allow_tags = True

    def __str__(self):
        print("------------------------------------")
        print(self.image.url)
        print("------------------------------------")
        return "%s" % (self.nombre)
    
    
    
    class Meta:
        db_table = 'Habitacion'
        verbose_name = 'Habitación'
        verbose_name_plural = 'Habitaciones'
