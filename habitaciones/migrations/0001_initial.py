# Generated by Django 4.2.6 on 2023-10-19 20:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='tipohabitacion',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo', models.CharField(max_length=80, unique=True, verbose_name='Tipo de habitacion')),
            ],
            options={
                'verbose_name': 'Tipo de habitación',
                'verbose_name_plural': 'Tipos de habitaciones',
                'db_table': 'tipohabitacion',
            },
        ),
        migrations.CreateModel(
            name='Habitacion',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, null=True, upload_to='fotos/habitaciones/', verbose_name='Ingrese imagen')),
                ('nombre', models.CharField(max_length=100, unique=True, verbose_name='Nombre')),
                ('costo', models.PositiveBigIntegerField(default=100, verbose_name='Costo')),
                ('descripcion', models.TextField(max_length=300, verbose_name='Descripción')),
                ('tipo_habitacion', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='habitaciones.tipohabitacion')),
            ],
            options={
                'verbose_name': 'Habitación',
                'verbose_name_plural': 'Habitaciones',
                'db_table': 'Habitacion',
            },
        ),
    ]
