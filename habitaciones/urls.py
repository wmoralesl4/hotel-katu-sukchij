from django.contrib import admin
from django.urls import path
from .views import HabitacionView
from . import views

app_name='habitaciones'
urlpatterns = [
    path('', HabitacionView.as_view(), name="habitacion")
]
